/* frequency.c
 * This file deals with frequency restrictions.
 */

#include <stdio.h>
#include <stdlib.h>

#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xaw/Command.h>
#include <Xaw/Form.h>
#include <Xaw/Label.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"
#include "game.h"
#include "utils.h"
#include "options.h"
#include "widgets.h"


Widget frequencyHigh,frequencyLow;


static char * freqAccel = 
 " <Key>Return:  update-frequency()";


/* MakeFrequency:
 *  make specialised "dialog box" that takes
 *  two input values, with labels next to each input area, and
 *  one make dialog label
 */
void MakeFrequency(Widget parent){
	Widget mainlabel,highlabel,lowlabel;
	XtAccelerators Accel;
	char highvalue[10] = "";
	char lowvalue[10] = "";

	
	
	if(lowfrequency !=0)
		sprintf(lowvalue,"%d",lowfrequency);
	if(highfrequency !=0)
		sprintf(highvalue,"%d",highfrequency);

	mainlabel = XtVaCreateManagedWidget("freqLabel",labelWidgetClass,parent,
			XtNlabel,"        Frequency Range",
			XtNborderWidth,0,
			NULL);

	highlabel = XtVaCreateManagedWidget(
			"freqhighLabel",labelWidgetClass,parent,
			XtNlabel,"High:",
			XtNfromVert,mainlabel,
			XtNborderWidth,0,
			NULL);
	frequencyHigh = XtVaCreateManagedWidget(
			"freqHigh",asciiTextWidgetClass,parent,
			XtNfromVert,mainlabel,
			XtNfromHoriz,highlabel,
			XtNeditType,XawtextEdit,
			XtNwidth,45,
			XtNstring,highvalue,
			NULL);
	lowlabel = XtVaCreateManagedWidget(
			"freqlowLabel",labelWidgetClass,parent,
			XtNlabel,"Low:",
			XtNfromVert,mainlabel,
			XtNfromHoriz,frequencyHigh,
			XtNborderWidth,0,
			NULL);
	frequencyLow = XtVaCreateManagedWidget(
			"freqLow",asciiTextWidgetClass,parent,
			XtNfromVert,mainlabel,
			XtNfromHoriz,lowlabel,
			XtNeditType,XawtextEdit,
			XtNwidth,50,
			XtNstring,lowvalue,
			NULL);

	Accel = XtParseAcceleratorTable(freqAccel);
	XtOverrideTranslations(frequencyHigh,Accel);
	XtOverrideTranslations(frequencyLow,Accel);
}



/* UpdateFrequency()
 *	Change frequency restrictions on range of kanji used
 */
void 
UpdateFrequency(Widget w,XEvent *event,String *params,Cardinal *num_parags){
	int *setthisvalue,oldvalue;

	if(w == frequencyHigh){
		setthisvalue = &highfrequency;
	} else if( w == frequencyLow){
		setthisvalue = &lowfrequency;
	} else {
		puts("??? Huh??? got UpdateFreq callback from imaginary widget??");
		exit(0);
	}

	oldvalue = *setthisvalue;
	*setthisvalue = GetWidgetNumberval(w);

	if(w == frequencyHigh){
		SetXtrmNumber("highfrequency", *setthisvalue);
	} else {
		SetXtrmNumber("lowfrequency", *setthisvalue);
	}

	CountKanji();
	if(numberofkanji <HAVE_AT_LEAST){
		setstatus("Too few kanji available!!");
		Beep();
		*setthisvalue = oldvalue;
		SetWidgetNumberval(w,oldvalue);
		/* and just reset numberofkanji value!! */
		CountKanji();
	} else {
		char numbuff[100];
		sprintf(numbuff,"%d characters active",numberofkanji);
		setstatus(numbuff);
	}
	
}


