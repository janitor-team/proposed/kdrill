/* GLOBAL externs file for kdrill!!
 * Hopefully, the only stuff that is in here, is stuff that should REALLY
 * be globally visible, not just any stuff that happens to be exported
 * by a particular file/module.
 */

extern Atom delete_message,wm_message;


/***********************************************************************
 * 'translations' is a mixed bag of context-sensitive stuff.
 *  Between MINKANJIALLOWED, and MAXKANJIALLOWED,
 *  the array index is actually the kanji's specific JIS number.
 *  Higher than that, and it is just a random arrayptr of
 *  translations in 'edict'
 ***********************************************************************/
extern TRANSLATION translations[MAXTRANSLATIONSALLOWED];



extern int lowestkanji,highestkanji,numberofkanji;/* keep track of how many we have */
extern int lowfrequency,highfrequency;		/* for frequency restrictions */

extern char usefilename[]; /*actual name of usefile*/

extern void usage();
extern void quit(Widget w, XtPointer, XtPointer);
extern void Beep();	/* nice generic routine that respects whether
			 * player wants quiet or not
			 */

/* in mainwindow.c, and we dont have mainwindow.h */
extern void ClearCheat();


/* other "usefull" general functions */
extern int xtoi(char *);
extern int getline(FILE *, unsigned char *);
extern void setstatus(char *);

extern int FindIndex(char *);


extern XFontStruct *largekfont;
extern XFontStruct *smallkfont;
extern XFontStruct *englishfont;
extern XFontStruct *defaultfont;



/* callbacks have been moved to their specific .h files */

extern XtAppContext Context;

extern char *homedir;

extern unsigned long black,white;
extern Display	*display;
extern Window	mainwindow,rootwindow;
extern GC	gc,cleargc;

/* buttons 'n' widgets */
extern Widget toplevel,form;
extern Widget options_popup;
extern Widget frequencyForm,frequencyHigh,frequencyLow;
extern Widget buttonform,englishform,kanjiform;

extern Widget choicesWidgets[NUMBEROFCHOICES],questionWidget;

extern Widget gradelevelForm,gradeButtons[];
extern Widget usefilebutton,orderbutton;


/* Accelerator functions.. */
extern void UpdateFrequency(Widget,XEvent *,String *,Cardinal *);
extern void DoFind(Widget,XEvent *,String *,Cardinal *);

/* misc resource-level stuffs... */
extern void GetXtrmString(char *, char *, char *);
extern int GetXtrmNumber(char *,char *);
extern Boolean GetXtrmBoolean(char *, char *);
extern void SetXtrmString(char *, char *);
extern void SetXtrmNumber(char *,int);
extern void SetXtrmBoolean(char *, Boolean);

/* These flags indicate current settings of the game */
extern Boolean useUsefile; /*	we Always read in a usefile, but
				this says if we actually use it */
extern Boolean doBell,showinorder;
extern Boolean showEnglish;
extern Boolean switchKanaEnglish;
extern int gradelevelflags;

#ifdef OLDLOG
extern FILE *logfile;
#endif

