/* externally usable routines to interface to the multiple-kanji popup window
 * (primarily used by the search window)
 */

extern void MakeMulti(); /* Init-time routine */

extern void ClearAllMulti();
extern void AddMultiTranslation(TRANSLATION);
extern int getMultiMax();
extern int getMultiCount();

extern void ShowMulti(); /* popup the multimatch window */

/* see game.h for GUESS_XXX mode defines */
/* call just before calling ShowMulti(), if you need to set mode  */
extern void SetMultiMode(int mode); 

extern void RefreshMultiLabels(); /* refresh labels */
