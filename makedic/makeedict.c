/* This program is for entering simple edict entries.
 *	Use it with the pregenerated infiles given.
 */




#include <stdio.h>




main(int argc, char *argv[]){
	FILE *outfile;
	int kindex, kindex2;
	char english[100];

	if(argc != 2){
		fprintf(stderr,"We need a filename for output\n");
		exit(0);
	}
	outfile = fopen(argv[1],"w");
	if(outfile == NULL){
		fprintf(stderr,"Sorry, could not open file\n");
		exit(0);
	}

	while (1) {
		char inbuf[100];
		char *parse;
		
		printf("Hex JIS char please (up to 2): ");
		fgets(inbuf,99,stdin);

		if(inbuf[0] == '#')
			continue;

		kindex2=0;
		sscanf(inbuf,"%x %x",&kindex, &kindex2);

		if(kindex == 0){
			break;
		}

		puts("Now one line of translation in english");
		fgets(inbuf,99,stdin);

		/* kill return */
		parse = inbuf;
		while(*parse != '\n'){
			parse++;
		}
		*parse = '\0';
		

		fputc( ((kindex & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex & 0xff) |  0x80, outfile);
		fputc( ((kindex2 & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex2 & 0xff) |  0x80, outfile);

		fprintf(outfile," [");
		fputc( ((kindex & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex & 0xff) |  0x80, outfile);
		fputc( ((kindex2 & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex2 & 0xff) |  0x80, outfile);
		fputc(']', outfile);

		
		fprintf(outfile," /%s/\n",inbuf);
	}
	puts("Done");
	fflush(outfile);
	fclose(outfile);
	exit(0);
}
