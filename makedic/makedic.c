/* This program is for entering kanjidic entries.
 *	However, it is hardcoded to work only for
 *	kana entries. (which arent proper entries anyway)
 *	Use it with the pregenerated infiles given.
 */



/*#define BOTHKANA  */
/*	If you define BOTHKANA == 1, it will make
 *	kdrill show both  hiragana and kanakana, if you switch over
 *	to "kana" mode in kdrill.
 *	Normally, switching to kana mode would give away this
 *	answer. This way still gives you the answer, but
 *	gives you another way to remember it.
 *	This would be usefile if you know either  katakana or hiragana
 *	thoroughly, and wish to associate kana with the same
 *	meanings
 */


#include <stdio.h>




main(int argc, char *argv[]){
	FILE *outfile;
	int kindex;
	char english[100];

	if(argc != 2){
		fprintf(stderr,"We need a filename for output\n");
		exit(0);
	}
	outfile = fopen(argv[1],"w");
	if(outfile == NULL){
		fprintf(stderr,"Sorry, could not open file\n");
		exit(0);
	}

	while (1) {
		char inbuf[100];
		char *parse;
		
		printf("Hex index please: ");
		fgets(inbuf,99,stdin);

		if(inbuf[0] == '#')
			continue;

		sscanf(inbuf,"%x",&kindex);

		if(kindex == 0){
			break;
		}

		puts("Now one line of pronunciation in english");
		fgets(inbuf,99,stdin);

		/* kill return */
		parse = inbuf;
		while(*parse != '\n'){
			parse++;
		}
		*parse = '\0';
		

		fputc((kindex & 0xff00)>>8, outfile);
		fputc((kindex & 0xff), outfile);

		fprintf(outfile," %x ",kindex);
		
		fputc( ((kindex & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex & 0xff) |  0x80, outfile);
#ifdef BOTHKANA
		fputc(' ',outfile);

		if(kindex
		kindex++;
		fputc( ((kindex & 0xff00)>>8) | 0x80, outfile);
		fputc((kindex & 0xff) |  0x80, outfile);
#endif

		fprintf(outfile," {%s}\n",inbuf);
	}
	puts("Done");
	fflush(outfile);
	fclose(outfile);
	exit(0);
	
}
