
/* options.c
 *   Handles the options window popup
 *   This handles the meat of the optiosn popup stuff
 */

#include <stdio.h>

#include <Xos.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Shell.h>
#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"
#include "game.h"
#include "grades.h"
#include "search.h"
#include "searchwidgets.h"
#include "learn.h"
#include "widgets.h"
#include "utils.h"
#include "log.h"
#include "multikanji.h"
#include "init.h"
#include "timeout.h"


Widget orderbutton,usefilebutton;
Widget kanjimode,englishmode,kanamode;

int buttonswidth=BUTTONSWIDTH;

Widget options_popup;
Widget optionsform;
Widget frequencyForm,gradelevelForm, timeoutForm;

/* Assume "True" == 1 !! */
char * orderlabel[2] = {
	"Showing randomly",
	"Showing in order",
};
char * romajilabel[2] = {
	"Kana/Romaji     ",
	"Romaji/Kana     ",
};

char * usefilelabel[2] = {
	"No    Usefile",
	"Using Usefile",
};

void ordercallback(Widget w,XtPointer data,XtPointer calldata)
{
	   showinorder = !showinorder;
	   XtVaSetValues(w,XtNlabel,orderlabel[(int) showinorder],NULL);
	   switch(showinorder){
		case 0:
			setstatus("Will now select kanji randomly");
			SetXtrmBoolean("showinorder", False);
			break;
		case 1:
			setstatus("Will now go through kanji in order of #");
			SetXtrmBoolean("showinorder", True);
			break;

	   }
	   return;
}

/* update the LABEL of the "using usefile" button in options window,
 * according to global useUsefile
 */
void UpdateUsefileL()
{
	XtVaSetValues(usefilebutton,XtNlabel,usefilelabel[(int) useUsefile],NULL);
}

/* Handle togglebutton in Options window, for "Use Usefile" */
void UsefileCallback(Widget w,XtPointer data,XtPointer calldata)
{
	char statbuff[300];

	switch(useUsefile){
		case True:
			useUsefile = False;
			setstatus("No longer restricting by usefile");
			CountKanji();
			break;

		case False:
			if(NumInUsefile() <HAVE_AT_LEAST){
				sprintf(statbuff,"only %d entries: cannot use",
						NumInUsefile());
				setstatus(statbuff);
				return;
			}
			
			useUsefile = True;
			CountKanji();
			if(numberofkanji<HAVE_AT_LEAST){
				setstatus("Too few kanji in usefile");
				useUsefile = False;
				CountKanji();
			} else {

				sprintf(statbuff,"Now using usefile %s",
					usefilename);
				setstatus(statbuff);
			}
			break;
	}

	SetXtrmBoolean("nousefile", !useUsefile);
	UpdateUsefileL();
	return;

}

/* Switch between showing true kana, and romaji.
 *  We then have to go and reset all the appropriate button labels. sigh.
 *  (In the MAIN window. Plus possibly other places...)
 */
void romajicallback(Widget w,XtPointer data,XtPointer calldata)
{
	romajiswitch=1-romajiswitch;
	
	switch(romajiswitch){
		case 1:
			setstatus("Will now print romaji");
			SetXtrmBoolean("romajiswitch", True);
			break;
		case 0:
			setstatus("Will now print kana");
			SetXtrmBoolean("romajiswitch", False);
			break;
	}
	XtVaSetValues(w,XtNlabel,romajilabel[(int)romajiswitch],NULL);

	/* Now go and reset ALL labels. Sigh. */
	printquestion();
	printallchoices();
	RefreshMultiLabels();
	SearchRefresh(); /* search window */
	DisplayLearnChar(); /* 'learn' window */
	/* kana popup window! */
	ksearch_toggles(w, (XtPointer)NULL, (XtPointer)1);

	return;
}
		   
/* This gets called after clicking on the "options" button,
 * or with the "o" keyboard accelerator (but not directly, with
 * the keyboard accelerator)
 */
void OptionsCallback(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int options_up = -1;
	static Position rel_x,rel_y;
	Position x,y;

	if(options_up==-1){
		/* first time init.. */
		rel_x = GetXtrmNumber("options_popupx","Options_popupx");
		rel_y = GetXtrmNumber("options_popupy","Options_popupy");
		options_up=0;
	}
	if(isMapped(options_popup)==False){
		XtTranslateCoords(toplevel,rel_x,rel_y,&x,&y);
		/* If you have a decent window manager, this should not
		 * be neccessary. But in a concession to those who
		 * still dont run something decent...
		 */
		if(x<0) x=0;
		if(y<0) y=0;
		
		XtVaSetValues(options_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);
		XtPopup(options_popup,XtGrabNone);
		if(options_up==0){
			setup_deletewindow(options_popup);
			options_up=1;
		}
		setstatus("Bringing up options window...");
	} else {
		XtPopdown(options_popup);
	}
}


/* MakeGradeSelect()
 *	create buttons to select grades 1-6, plus "all" and "+"  buttons
 */
void MakeGradeSelect(Widget parent){
	Widget gradelevelLabel;
	
	int widgetcount;

	gradelevelLabel = XtVaCreateManagedWidget(
			"gradelabel",labelWidgetClass,parent,
			XtNlabel,"        Grade Select",XtNborderWidth,0,
			NULL);

	gradeButtons[1] = XtVaCreateWidget("grade1",commandWidgetClass,parent,
			 XtNlabel,"1",
			 XtNfromVert,gradelevelLabel,NULL);

	gradeButtons[2] = XtVaCreateWidget("grade2",commandWidgetClass,parent,
			 XtNlabel,"2",XtNfromHoriz,gradeButtons[1],
			 XtNfromVert,gradelevelLabel,NULL);
	gradeButtons[3] = XtVaCreateWidget("grade3",commandWidgetClass,parent,
			 XtNlabel,"3",XtNfromHoriz,gradeButtons[2],
			 XtNfromVert,gradelevelLabel,NULL);
	gradeButtons[4] = XtVaCreateWidget("grade4",commandWidgetClass,parent,
			 XtNlabel,"4",XtNfromHoriz,gradeButtons[3],
			 XtNfromVert,gradelevelLabel,NULL);
	gradeButtons[5] = XtVaCreateWidget("grade5",commandWidgetClass,parent,
			 XtNlabel,"5",XtNfromHoriz,gradeButtons[4],
			 XtNfromVert,gradelevelLabel,NULL);
	gradeButtons[6] = XtVaCreateWidget("grade6",commandWidgetClass,parent,
			 XtNlabel,"6",XtNfromHoriz,gradeButtons[5],
			 XtNfromVert,gradelevelLabel,NULL);
	gradeButtons[7] = XtVaCreateWidget("grade+",commandWidgetClass,parent,
			 XtNlabel,"+",XtNfromHoriz,gradeButtons[6],
			 XtNfromVert,gradelevelLabel,NULL);

	gradeButtons[0] = XtVaCreateWidget("allgrades",commandWidgetClass,parent,
			  XtNlabel," All ", XtNfromHoriz,gradeButtons[7],
			  XtNfromVert,gradelevelLabel,NULL);

	XtManageChildren(gradeButtons,8);

	for(widgetcount=0;widgetcount<8;widgetcount++){
		XtAddCallback(gradeButtons[widgetcount],XtNcallback,
			      GradeCallback,(XtPointer) widgetcount);
	}
	/* set labels properly */
	printgrades();
	/* select ALL.. */
	
}



void MakeOptionsPopup()
{
	Widget q[3], romajibutton;
	Widget Opshowform, Opguessform;
	if(defaultfont!=NULL) {
		XCharStruct deffontsize=defaultfont->max_bounds;
	
		buttonswidth=12*(deffontsize.ascent+deffontsize.descent);
	}

	options_popup = XtVaCreatePopupShell("kdrill_options",
		transientShellWidgetClass,
		toplevel,
		NULL);

	optionsform = XtVaCreateManagedWidget("optionsform",
					      formWidgetClass,
					      options_popup,
					      NULL);

	Opshowform = XtVaCreateManagedWidget("Opshowform",
					      formWidgetClass,
					      optionsform,
					      NULL);



	q[0] = XtVaCreateManagedWidget(
			"q0",commandWidgetClass, Opshowform,
			XtNlabel,"Show kanji",
			XtNvertDistance,8,
			XtNshapeStyle,XawShapeOval,
			NULL);
	q[1] = XtVaCreateManagedWidget(
			"q1",commandWidgetClass, Opshowform,
			XtNlabel,"Show English",
			XtNshapeStyle,XawShapeOval,
			XtNfromVert,q[0],
			NULL);
	q[2] = XtVaCreateManagedWidget(
			"q2",commandWidgetClass, Opshowform,
			XtNlabel,"Show meaning",
			XtNshapeStyle,XawShapeOval,
			XtNfromVert,q[1],
			NULL);


	Opguessform = XtVaCreateManagedWidget("Opguessform",
					      formWidgetClass,
					      optionsform,
					      XtNfromVert, Opshowform,
					      NULL);



	kanjimode = XtVaCreateManagedWidget(
			"kanjimode",commandWidgetClass, Opguessform,
			XtNlabel,"Guess kanji",
			XtNvertDistance,8,
			XtNshapeStyle,XawShapeOval,
			NULL);

	englishmode = XtVaCreateManagedWidget(
			"englishmode",commandWidgetClass, Opguessform,
			XtNlabel,"Guess English",
			XtNfromVert,kanjimode,
			XtNshapeStyle,XawShapeOval,
			NULL);

	kanamode = XtVaCreateManagedWidget(
			"kana",commandWidgetClass, Opguessform,
			XtNlabel,"Guess meaning",
			XtNshapeStyle,XawShapeOval,
			XtNfromVert,englishmode,
			NULL);





	orderbutton = XtVaCreateManagedWidget("order",commandWidgetClass,
					      optionsform,
			XtNlabel,orderlabel[(int) showinorder],
			XtNfromHoriz,Opshowform,
			NULL);

	romajibutton = XtVaCreateManagedWidget("romaji",commandWidgetClass,
					       optionsform,
			XtNlabel,romajilabel[(int) romajiswitch],
			XtNfromVert,orderbutton,
			XtNfromHoriz,Opshowform,
			NULL);

	usefilebutton = XtVaCreateManagedWidget("usefile",commandWidgetClass,
						optionsform,
			XtNlabel,usefilelabel[(int) useUsefile],
			XtNfromVert,romajibutton,
			XtNfromHoriz,Opshowform,
			NULL);







	gradelevelForm = XtVaCreateManagedWidget(
			"gradeForm",formWidgetClass,optionsform,
			XtNhorizDistance,GRADEDISTANCE,
			XtNvertDistance,10,
			XtNfromHoriz,orderbutton,
			NULL);

	MakeGradeSelect(gradelevelForm);

	frequencyForm = XtVaCreateManagedWidget(
			"freqForm",formWidgetClass,optionsform,
			XtNvertDistance,10,
			XtNfromHoriz,Opguessform,
			XtNfromVert, Opshowform,
			NULL);

	MakeFrequency(frequencyForm);

	timeoutForm = XtVaCreateManagedWidget(
			"timeoutForm",formWidgetClass,optionsform,
			XtNvertDistance,10,
			XtNfromHoriz,frequencyForm,
			XtNfromVert, Opshowform,
			NULL);

	MakeTimeoutOptions(timeoutForm);

	XtAddCallback(q[0],XtNcallback,ChangeQuestion,(XtPointer) GUESS_KANJI);
	XtAddCallback(q[1],XtNcallback,ChangeQuestion,(XtPointer) GUESS_ENGLISH);
	XtAddCallback(q[2],XtNcallback,ChangeQuestion,(XtPointer) GUESS_KANA);

	XtAddCallback(kanjimode,XtNcallback,ChangeMode,(XtPointer) GUESS_KANJI);
	XtAddCallback(englishmode,XtNcallback,ChangeMode,(XtPointer) GUESS_ENGLISH);
	XtAddCallback(kanamode,XtNcallback,ChangeMode,(XtPointer) GUESS_KANA);
	XtAddCallback(orderbutton,XtNcallback,ordercallback,NULL);
	XtAddCallback(usefilebutton,XtNcallback,UsefileCallback,NULL);
	XtAddCallback(romajibutton,XtNcallback,romajicallback,NULL);

	XtSetKeyboardFocus(options_popup,toplevel);
}
