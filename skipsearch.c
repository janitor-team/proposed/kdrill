/* since were having problems with the SKIP window, I needed to split it out */

#include <stdio.h>

#include <Xos.h>
#include <Xfuncs.h>
#include <Intrinsic.h>
#include <StringDefs.h>

#include <Shell.h>

#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"

#include "searchwidgets.h"
#include "search.h"
#include "convert.h"
#include "game.h"
#include "utils.h"
#include "init.h"
#include "readfile.h"


/****************************************************/
/* Now, "SKIP" method */
/****************************************************/


static Widget SKIPinput_popup;

Widget SKIPcategories[4];
static int SKIPchoice=0; /* This is the "type" of SKIP value we are using
			  * It is the first number in the X-Y-Z SKIP format
			  * 
			  */

Widget SKIPinput1,SKIPinput2;
Widget SKIPnumberinput[2];/* if SKIP type 1-3, input number */
Widget SKIPnumberlabel[2];
Widget SKIPshapeinput[4]; /* if SKIP type 4, click on closest shape */




void setSKIPlabels(int choice){
	SKIPchoice=choice;

	switch(choice){
	   case 0:
		XtVaSetValues(SKIPnumberlabel[0], XtNlabel,
			      "Number of strokes on left ",NULL);
		XtVaSetValues(SKIPnumberlabel[1], XtNlabel,
			      "Number of strokes on right",NULL);
		XtSetSensitive(SKIPshapeinput[0], False);
		XtSetSensitive(SKIPshapeinput[1], False);
		XtSetSensitive(SKIPshapeinput[2], False);
		XtSetSensitive(SKIPshapeinput[3], False);
		break;
	   case 1:
		XtVaSetValues(SKIPnumberlabel[0], XtNlabel,
			      "Number of strokes on top   ",NULL);
		XtVaSetValues(SKIPnumberlabel[1], XtNlabel,
			      "Number of strokes on bottom",NULL);
		XtSetSensitive(SKIPshapeinput[0], False);
		XtSetSensitive(SKIPshapeinput[1], False);
		XtSetSensitive(SKIPshapeinput[2], False);
		XtSetSensitive(SKIPshapeinput[3], False);
		break;
	   case 2:
		XtVaSetValues(SKIPnumberlabel[0], XtNlabel,
			      "Number of strokes on outside",NULL);
		XtVaSetValues(SKIPnumberlabel[1], XtNlabel,
			      "Number of strokes on inside ",NULL);
		XtSetSensitive(SKIPshapeinput[0], False);
		XtSetSensitive(SKIPshapeinput[1], False);
		XtSetSensitive(SKIPshapeinput[2], False);
		XtSetSensitive(SKIPshapeinput[3], False);
		break;
	   case 3:
		XtVaSetValues(SKIPnumberlabel[0], XtNlabel,
			      "Number of total strokes   ",NULL);
		XtVaSetValues(SKIPnumberlabel[1], XtNlabel,
			      "**Choose shape from below*",NULL);
		XtSetSensitive(SKIPshapeinput[0], True);
		XtSetSensitive(SKIPshapeinput[1], True);
		XtSetSensitive(SKIPshapeinput[2], True);
		XtSetSensitive(SKIPshapeinput[3], True);
		break;
	}
}



void SKIPshapecallback(Widget w,XtPointer client_data, XtPointer call_data){
/*	int shapenum=(int)client_data;*/
	
	char newlabel[3];
	sprintf(newlabel,"%1d",(int)client_data);
	XtVaSetValues(SKIPnumberinput[1], XtNstring,   newlabel, NULL);
}

/* Gets called when user selects which of the four main categories
 * a kanji matches.
 * We read the current values in the text input fields, but use the
 * cached value of SKIPchoice.
 * Then we call doskipfind()
 */
void SKIPcallbacks(Widget w,XtPointer client_data, XtPointer call_data)
{
	int scount;
	int skipnum=(int)client_data;

	if(skipnum==10) {
		int s1,s2,s3;

		s1=SKIPchoice+1;

		s2=GetWidgetNumberval(SKIPnumberinput[0]);
		s3=GetWidgetNumberval(SKIPnumberinput[1]);

#ifdef DEBUG
		printf("s1=%d,s2=%d,s3=%d\n",s1,s2,s3);
#endif

		if((s1==0)) {
			setstatus("Insufficient values set");
			puts("BAD!");
			return;
		}

		doskipfind(skipfromthree(s1,s2,s3));
		return;
	}

	for(scount=0;scount<4;scount++) {
		if(scount==skipnum) {
			HighlightButton(SKIPcategories[scount]);
		} else {
			UnhighlightButton(SKIPcategories[scount]);
		}
	}
	setSKIPlabels(skipnum);
}

/* This exists solely to make asciiTextWidgetClass inputs ignore RETURN */
/* It gets initialized in MakeSKIPInputPopup */
static XtAccelerators ignoreAccel;
static char *ignoreAccelMap =
 " <Key>Return:  do-nothing()"; 



/* try to make the skip search widgets.
 * This is passed the top-level form. SO it is responsible for basically
 * EVERYTHING on the skip search window:
 *  -- The Top-FOUR skip mode buttons
 *  -- the help message
 *  -- the two "upper/lower" stroke count bits plus labels
 *  -- the last SKIP-factor quad input buttons
 *  -- the search button
 *
 * 
 */
void makeskipinput(Widget parent)
{
	XCharStruct charstruct = englishfont->max_bounds;
	int scount;
	char tmpname[30];
	XChar2b cat_string[10][2]=
	{
		{{0x21, 0x43}, {0,0}}, /* graphic glyphs for top buttons*/
		{{0x21, 0x3d}, {0,0}},
		{{0x22, 0x22}, {0,0}},
		{{0x21, 0x7a}, {0,0}},
		{{0,0}},
		/* graphic glyphs for bottom buttons */
		{{0x28, 0x40}, {0,0}},
		{{0x28, 0x3b}, {0,0}},
		{{0x22, 0x22}, {0,0}},
		{{0x22, 0x23}, {0,0}},
		{{0,0}},
		
	};
	
	Widget SKIPdirections, search;


	int wcount;
	for(wcount=0;wcount<4;wcount++){
		sprintf(tmpname, "skiptopb-%d",wcount);
		SKIPcategories[wcount]=
			XtVaCreateManagedWidget(tmpname, commandWidgetClass,
						parent,
					 XtNencoding, XawTextEncodingChar2b,
					 XtNfont, largekfont,
					 XtNlabel, cat_string[wcount],
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
					 NULL);
		if(wcount>0){
			XtVaSetValues(SKIPcategories[wcount],
				      XtNfromHoriz,SKIPcategories[wcount-1],
				      NULL);
		}
	}

	SKIPdirections = XtVaCreateManagedWidget(
			"directions", asciiTextWidgetClass, parent,
			XtNfromVert, SKIPcategories[0],
			XtNdisplayCaret, False,
			XtNwidth, 46*charstruct.width,
			XtNheight, 14*(charstruct.ascent+charstruct.descent),
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			XtNstring,
"This is the SKIP kanji search window.\n"
"First, choose from the top whether the \n"
"kanji looks;\n"
"  Divided in half vertically,\n"
"  Divided in half horizontally,\n"
"  Enclosed somehow,\n"
"  or 'something else'.\n"
"\n"
"Then enter the number of strokes on \n"
"the left, then rightside.\n"
"Or strokes on top, then bottom.\n"
"Or pick closest match to type.\n"
"Then press the Search button",
			XtNfont,englishfont,
			NULL);

	SKIPinput1=XtVaCreateManagedWidget("skipinputform1", formWidgetClass,
					   parent,
					   XtNfromVert,SKIPdirections,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
					   NULL);


	SKIPnumberinput[0] =
		XtVaCreateManagedWidget("ninput1", asciiTextWidgetClass,
					SKIPinput1,
				XtNlabel, "",
				XtNwidth,30,
				XtNeditType,XawtextEdit,
				NULL);
	SKIPnumberinput[1] =
		XtVaCreateManagedWidget("ninput2", asciiTextWidgetClass,
					SKIPinput1,
				XtNfromVert, SKIPnumberinput[0],
				XtNlabel, "",
				XtNwidth,30,
				XtNeditType,XawtextEdit,
				NULL);

	XtOverrideTranslations(SKIPnumberinput[0],ignoreAccel);
	XtOverrideTranslations(SKIPnumberinput[1],ignoreAccel);


	/* Yes, these are supposed to be to the RIGHT of the above */
	SKIPnumberlabel[0] =
		XtVaCreateManagedWidget("nlabel1", labelWidgetClass,
					SKIPinput1,
				XtNfromHoriz, SKIPnumberinput[0],
				XtNlabel, "______________________________",
				NULL);
	SKIPnumberlabel[1] =
		XtVaCreateManagedWidget("nlabel2", labelWidgetClass,
					SKIPinput1,
				XtNfromHoriz, SKIPnumberinput[0],
				XtNfromVert, SKIPnumberlabel[0],
				XtNlabel, "______________________________",
				NULL);



	SKIPinput2=XtVaCreateManagedWidget("skipinputform2", formWidgetClass,
					   parent,
					   XtNfromVert,SKIPinput1,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
					   NULL);

	
	for(wcount=0;wcount<4;wcount++){
		sprintf(tmpname, "skipshape-%d",wcount);
		SKIPshapeinput[wcount]=
		XtVaCreateManagedWidget(tmpname, commandWidgetClass, SKIPinput2,
		     XtNencoding, XawTextEncodingChar2b,
		     XtNfont, largekfont,
		     XtNlabel, cat_string[5+wcount],
		     XtNsensitive,False,
		     NULL);
		if(wcount>0){
		     XtVaSetValues(SKIPshapeinput[wcount],
				   XtNfromHoriz, SKIPshapeinput[wcount-1],
		     NULL);
		}
	}


	/* What good is this? WHy did I have this?
	clear = XtVaCreateManagedWidget(
			"clear", commandWidgetClass, parent,
			XtNfromVert, SKIPinput2,
			NULL);
	*/

	search = XtVaCreateManagedWidget(
			"skipsearchstart", commandWidgetClass, parent,
			XtNfromVert, SKIPinput2,
			XtNfont, englishfont,
			XtNlabel, "Search",
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
			NULL);



	XtAddCallback(search,XtNcallback, SKIPcallbacks,(XtPointer) 10);

	for(scount=0; scount<4; scount++) {
		XtAddCallback(SKIPcategories[scount],
			      XtNcallback, SKIPcallbacks,
			      (XtPointer) scount);
	}

	XtAddCallback(SKIPshapeinput[0],XtNcallback, SKIPshapecallback,
			      (XtPointer) 1);
	XtAddCallback(SKIPshapeinput[1],XtNcallback, SKIPshapecallback,
			      (XtPointer) 2);
	XtAddCallback(SKIPshapeinput[2],XtNcallback, SKIPshapecallback,
			      (XtPointer) 3);
	XtAddCallback(SKIPshapeinput[3],XtNcallback, SKIPshapecallback,
			      (XtPointer) 4);

#ifdef NONONON
#endif /* NONONON */
	
}

/* exported routine */
void MakeSKIPInputPopup()
{
	Widget skipinputform;

	ignoreAccel = XtParseAcceleratorTable(ignoreAccelMap);

	SKIPinput_popup = XtVaCreatePopupShell("kdrill_skipsearch ",
		/*transientShellWidgetClass,*/
		topLevelShellWidgetClass,
		search_popup,
		NULL);

	skipinputform = XtVaCreateManagedWidget("skipinputform",
					     formWidgetClass,
					     SKIPinput_popup,
					     NULL);

	/* make the 10 input things, plus 4 display things */ 
	makeskipinput(skipinputform);
}

/* This gets called after clicking on the "Kanji search" button.
 * It pops up the SKIP input window
 */
void ShowSKIP(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int skip_up = -1;

	static Position rel_x,rel_y;
	Position x,y;


	if(skip_up==-1){
		/* first time init.. */
		rel_x = 10;
		rel_y = 10;
		skip_up=0;
	}
	if(isMapped(SKIPinput_popup)==False){

		XtTranslateCoords(search_popup,rel_x,rel_y,&x,&y);
		XtVaSetValues(SKIPinput_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);
		XtPopup(SKIPinput_popup,XtGrabNone);
		if(skip_up==0){
			setup_deletewindow(SKIPinput_popup);
			skip_up=1;
		}
		setstatus("Bringing up SKIP input window...");
	} else {
		XtPopdown(SKIPinput_popup);
	}
}
