/*
 * defs.h for kdrill
 *   Okay, I "lied".. this file has struct definitions, as
 *   well as the #defines
 *
 * See "externs.h" for the main global variables.
 */

#include <Xmd.h> /* for byte word width */
#include <Xlib.h> /* for XChar2b */

#if defined(BSD) || defined(__DARWIN__)
/* It's Really Important that you use either lrand48(), or random()
 * The old rand() function is just not random enough
 */
# define srand(a) srandom(a)
# define lrand48() random()

#endif /* BSD */

/* for reading in kanjidic lines */
#define MAXLINELEN 1024

#define NOKANJI 0x2266

/* when in random mode, if you have incorrect kanji guessed,
 * will specifically pick an incorrectly-guessed kanji
 * probability of (1/REPEACTFRACTION)
 */
#define REPEATFRACTION 4
/* number of times a missed kanji will be repeated on purpose */
#define REPEATTIMES 2
#define MAXREPEAT 10
/* how many possible answers are there? */
/* maximum of 9, since you can only have accelerator keys 1-9 */
#define NUMBEROFCHOICES 5

#define STATUSDISTANCE	10
#define BUTTONSWIDTH	120	/* for options buttons */
#define INPUTWIDTH	50
#define GRADEDISTANCE	14

#define ALLGRADES 0xfe
#define ABOVESIXGRADE 0x80
/* FULLWIDTH is startup size of an english translation button */
#define FULLWIDTH 370
#define KANJIWIDTH 50

/* MAXTRANSLATIONSALLOWED reflects the size of an array of pointer-to-translation,
 * and an array of booleans for the Usefile
 * We malloc for every actual entry read
 *
 * MIN/MAX KANJIALLOWED is a special subset of that.
 */
#define MINKANJIALLOWED 0x2000
#define MAXKANJIALLOWED 0x8000 /* start edict at this+1 */
/* Crud. edict actually overflowed when MAXTRANSLATIONS==0x18000 */
#define MAXTRANSLATIONSALLOWED 0x30000


/* MAXRADICALS is the approximate number of radicals that we know of, that
 * may be building blocks for a particular kanji.
 * DO NOT CONFUSE THIS with the number of kanji that may be
 * associated with a particular radical.
 * Which for some reason, I named MAXKRADPOST
 */
#define MAXRADICALS 250

#define MAXKRADPOST 3200

/* The following means the minimum active translations we must have available.
 * Otherwise, we assume an error in configuration
 */
#define HAVE_AT_LEAST 9

typedef struct translationstruct * TRANSLATION;

struct translationstruct {
	int kdrill_index; /* position in internal array */

	char *english;	/* english translation string.
			 * null-terminated.
			 * Can theoretically be any length, but
			 * since it will all be on a single-line
			 * text widget, that would be silly
			 */

	CARD16 Uindex;		/* because it seems to be the future */
				/* "Unicode" index */

	BITS16 Qindex;		/* for the "four corner" lookup method */
	BITS16 Sindex;		/* for SKIP: 2 bytes, see skipfromthree() */
	BITS16 Nindex;		/* for annoying "See Nxxxx" references */
				/* Nelson dictionary */
	BITS16 Hindex;		/* Halpern dictionary indexes */

	BITS16 frequency;		/* frequency that kanji is used */
	BYTE grade_level;	/* akin to  school class level */

	BYTE Strokecount;

	BITS16 incorrect;

	
	XChar2b *pronunciation;	/* kana, actually */
	XChar2b *kanji;		/* can be pointer to pronunciation */

	TRANSLATION nextk;

};




/********************************************************************

SO here's the format for the translation/dictionary file I have decided
on so far:

I changed specs. it will have to be in "kanjidic" format...

Two char high-bit-set identifier, followed by JIS number in hex.
Followed by stroke information? [HAS TO BE TERMINATED BY 'Gx', where
				  'x' is any number]
Followed by pronunciation guide, in kana.
Followed by various english translations, enclosed in {}

"kanjidic" often has multiple {} phrases. which leads to another nasty
hack  in readstructs()

*/
