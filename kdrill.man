.\" kdrill.1 2003/08/21
.TH KDRILL 1 " Auguest 21st, 2003" "Phil\'s Software"
.SH NAME
kdrill v6.2 \- drill program for kanji chars under Xwindows 
(X11R5 or better is required to run)
kdrill also does dictionary lookup


Yikes.. this man-page is getting huge. But I am a great believer in
having proper documentation. Hopefully, this new format will help instead
of hinder.

At some future point in time, I shall convert this huge beast to HTML. But
that point is not now.
[Although actually, you CAN go to http://www.bolthole.com/kdrill/ for
 some help ]

TIP:  "/WORD" usually takes you to the next occurrence of "WORD",
if you are viewing this using a "man"-like program.

.SH SECTIONS (of this man page)
 RUNTIME OPTIONS
 RESOURCES
 DESCRIPTION
 PLAYING
 PLAYING OPTIONS
 LEARNING NEW CHARS
 USEFILES
 SEARCH
 KEYBOARD ACCELERATORS
 CONFIGURATION DETAILS
 LOGFILES AND MISSED KANJI
 KANJIDIC and EDICT

.SH RUNTIME OPTIONS

[Note: most of these options are now somewhat redundant. Kdrill now auto-saves
 its options. But just in case you want to know about these commandline
 options...]

.TP
.B \-usefile \fINewUsefileName\fR
Change name of usefile, which lets you drill on specific characters.
.TP
.B \-nousefile
Still read in usefile if it exists, but ignore it at startup.
.TP
.B \-kdictfile \fIOtherKanjidicFile\fR
Use a different dictionary file name. You may have "hira.dic" or "kata.dic"
installed, as well as "kanjidic", for example.
.TP
.B \-edictfile \fIOtherKanjidicFile\fR
Use a different edict-style-dictionary file name. "none" for no edict.
.TP
.B \-englishfont \fIFontName\fR
Changes only english display of english-guess buttons.
.TP
.B \-kanjifont \fIKanjiFontName\fR
Change large kanji font.
.TP
.B \-smallkanji \fIKanjiFontName\fR
Change small kanji/kana font for kana-guess buttons.
.TP
.B \-noBell 
Turns off beep on wrong answer.
.TP
.B \-guessmode
 say whether you want the guess choices to be in "english", "kanji", or "kana"
.TP
.B \-questionmode
 say whether you want the 'question' to be in "english", "kanji", or "kana"

.TP
.B \-showinorder
Start in ordered mode. Go through desired kanji in order of #.
.TP
.B \-gradelevel \fI<level #s>\fR
Start with different grade levels enabled. A string with one or more of [123456+]
.TP
.B \-showkana
Start with kana meanings instead of english.
.TP
.B \-lowfrequency #, \-highfrequency #
Set lowest and/or highest frequency kanji you want to see.
.TP
.B \-logfile \fIfilename\fR
Change filename to log current errors to (with "Log" button)
.TP
.B \-notallT
Don't insist that all dictionary entries have kana AND English.
\fBWARNING!\fR Normal operation is to ignore incomplete entries, and
thereby enable switching from kana to english without changing the quiz
kanji. Using this option will make kdrill move to another kanji if you
switch kana to English or vica versa.
 \[: default behaviour currently loses 300 kanji, with the kanjidic
file I have currently. All characters with Frequency ratings have full
translations. \]

.SH RESOURCES
Kdrill now saves config options in $HOME/.kdrill, in X-resource format.
The latest configuration will automatically be saved when you quit
kdrill normally.
If you want to change kdrill's settings, and you dont see a way to do it
in the options popup, you can probably change it in the 
global "KDrill" resource file, or in your personal "$HOME/.kdrill" file.
See the sample "KDrill" file for more detail, which is
often installed in /usr/lib/X11/app-defaults/KDrill, or someplace similar.
Values in $HOME/.kdrill will override the global settings.

.SH Colors
You may change the background of the windows using a resource file,
as mentioned above.

.SH DESCRIPTION

.B kdrill
is a program to drill users on meanings of kanji characters. Various
formats of drills are available:


   kanji   --\\ /--- kanji
   kana     --*--   kana
   english --/ \\--  english


.SH PLAYING
kdrill will present you with a kanji (or kana or english phrase)
and five possible meanings for it. Your goal is to guess which
one matches the kanji at the top. Initially, it will choose randomly from the
\fBentire dictionary\fR, so you will probably want to narrow the range, via
the OPTIONS section, below.

Any grade level or frequency rating the current kanji has will be
displayed in the top right hand side of the window, next to the "G:" and
"F:" letters. The kanji index number will be displayed after the "#:" sign. 

Click with your primary mouse button (usually the left one) on one of the multiple-choice
answered to see how well you know the lone kanji or meaning.  You may
also use the number keys to make your choice.
[1,2,3,4,5]

If you guess correctly, you will move on to another character. If you
guess incorrectly, you will have to guess again. Furthermore, kdrill will
make a note that you didn't know either the character displayed, or the
character for the incorrect meaning you clicked on.

If you are playing in random order, kdrill will randomly repeat the ones
you have missed. You will have to get a missed character right twice for
kdrill to think you know it. If you miss a character more than once, you
will have to repeat the character two times the number of times you
missed it. If you are playing in order, kdrill will keep to the
order, and not go back. It will still remember ones you have missed,
however, and will go back to them if you later switch to random order.

There are two ways of "cheating", if you are learning new characters, and
don't want to have an incorrect guess recorded. One way is to press the
"cheat" button, and the correct answer will be highlighted. The other way
is to make a guess with button 2 on your mouse. The character
of the one you clicked on will appear in the search window.
If the search window was not already open, it will appear when you do
this.

.SH PLAYING OPTIONS
If you want to change the way the game works while playing, you can bring
up the options window by pressing the options button. If you know how you
want the game to play before starting it, you can most likely do what you
want with a command-line option, described at the top of this
man-page. If you want to permanently change an option, see the
"RESOURCES" header, above.

The following options are to help narrow down the range of kanji you
get quizzed on. 

.TP
Grades

You may specify which grade levels you wish to study, by clicking on the
buttons labeled: "1", "2", "3", "4", "5", "6", "+", or "All", in the
"Grade Select" window. You may also select/unselect a grade by holding
down shift, followed by "1", "2", "3", "4", "5", "6", "+", or "a", in the
main window.

The "All" feature will select all grades. but it will not unselect them.

The default is to have all the grades enabled.

.TP 
 Frequency

Some kanji have frequency ratings. That means that, in real life, some
kanji are used more frequently than others. Frequency rating 1 means that
this is \fBthe\fR most frequently used character.  
The frequency of the true answer you are guessing will be displayed in
the top right of the main window, next to the "F:" sign, if a frequency
rating exists. 

The frequency range area in the options window allows you to limit the
kanji you see based on their frequency rating. The frequency range area
consists of two smaller input areas; "High", and "Low". 
.I High
means a kanji that is high frequency. That is to say, something that is
used often. According to the definitions of the dictionary, "1" means the
kanji that is used the \fImost\fR often. A frequency rating of "2" means
that the character has a \fIlower\fR frequency than "1". The most
frequently used kanji is the character for "day", which is "F: 1".

Setting a number in the "High" window limits high frequency kanji.
If there is a number in the "High" window, that means that you will see no
kanji that is of higher frequency than that number. Similarly, setting a
number in the "Low" window means that you will see no kanji of frequency
lower than that number. 2000 is a "Low" frequency kanji. If you put 2000
in the "Low" area, you would see nothing of lower frequency than the
character rated at 2000 (which happens to be "hazy")
that you would see nothing

A blank in the "High" or "Low" fields indicates no limit in the field. If
you try to set either window to "0", it will automatically set itself
blank for you. 

.TP
Order

It is possible to be drilled on kanji in order, without repetition, until
"all" kanji have been covered. Any restrictions on grade level or
frequency will still apply. To enable or disable ordering, click on the
"Showing randomly" (or "Showing in order") button in the options window.

\fINote that "in order" does not mean in order of frequency.\fR It means
in the order represented by the dictionary, denoted by the '#' number
shown at top right. This happens to be the JIS-encoding of the Kanji,
which we also call the kanji index.

.TP
\fBTIP\fR: A good way to start learning a range of chars, is to select the
"in order" option, and a particular grade level and/or frequency range. 
Press 'C' (Shift-c) for super-cheat. This will both highlight the correct
answer, AND show the full kanji+kana+english meanings in the popup
search window!

When you have looked at it enough, click on the correct answer to move on to
the next kanji.

Or rather than use the super-cheat option repeatedly... Read the next section.

.SH LEARNING NEW CHARS

If you would like a small little window to memorize new chars in, instead of
the bulkier 'main' or 'search' windows, there is now a 'learn' window.
Pressing the (learn) button on the main window will bring up the learn window, 
which only displays kanji, kana, and english meanings of a char.
Pressing one of the 'next' buttons will select a new char for you to look at, 
using the same rules of choosing that the main window uses.
(grade levels, and an optional usefile)

.SH USEFILES

A usefile is a way to tell kdrill "I want to be quizzed on these
kanji, and ONLY these specific kanji". Generally speaking, it is
easier to just pick a particular grade level or frequency range to
quiz yourself on. But if you know you want specific kanji (for
example, to study for a class!) having a usefile is very useful.

Grade and Frequency restrictions will apply, even if you have a
usefile. Thus, if all your usefile-defined kanji are of grade 4 or
higher, and you have only selected grades 3 and lower, kdrill will
complain that there are not enough kanji available, and attempt to add
viewable grade levels until there are enough value kanji to quiz on

To add or remove a kanji from the "usefile", pull up the search
window, and view a particular kanji. The "usefile" button at the far
right will be highlighted if it is in the usefile list. You can toggle
the button to set the status as you wish.

If you want to see all the kanji in your usefile, click on the "show"
button, below the "usefile" toggle, in the search window. It will then
show you the current list, and pressing on one will display it in the
search window. You can then remove it via the "usefile" toggle if you like.

When you quit kdrill, it will update the usefile, IF you have a minimum number
in the list (currently, 10). If you want to know if you have enough, 
use the options window to toggle "No Usefile" to "Using Usefile". It will
not let you, if there are not enough characters in the list.

If you wish kdrill to ignore your usefile when you start it up, you may use the
\-nousefile option. 

.TP
Usefile format

If you want to edit a usefile by hand, this is the format:

A usefile consists of a list of hex numbers; one per line, no initial
spaces allowed. A usefile lets the program know you are interested in
certain kanji, from the thousands listed in the dictionary.
It is possible to add comment lines by having the very first character
of a line be "#". However, those will be overwritten if you make changes
from within the program.
Hex numbers can be checked or found by using the "xfd" util on the "kanji24" 
font. 
Alternatively, you could use the search window or main kdrill window.
In on of the "#" input boxes, type in "0x", and then the hex number.
It is best to do this in the search window, since the main window may
have range restrictions on it.


.SH SEARCH

It is now possible to search for a character in kdrill. You may search for
an English phrase, a kana phrase, or a particular kanji.

kdrill will automatically show the first match. If there is more than one
match, it will be shown in a secondary popup window.
That window can be changed to display the english, kanji, or kana meaning of
each dictionary entry. Click on one to have it displayed in the main search
window.

Additionally, if a search turns up a kanji phrase instead of a single kanji,
you may click on the phrase at the top of the search window, to have the
secondary multi-listing window display the individual kanji for you to examine
in further detail.

.TP
English search

First, bring up the search window by pressing the search button. Then,
enter an English word (or fragment) in the bottom-most section of the
window, and press return or enter. The window will then display the first
kanji it finds that has that word in its definition, along with its
index number, grade, and other information available, if any.

.TP
 Kana search

If you want to search for a \fBkana\fR phrase, you now have TWO options!

For more experienced users, you can finally type in that tempting
kana window. There is no little ^ cursor, but dont worry about that.
\fBDO\fR worry about the following conventions:

  Type "n " (n,space) to convert a ending 'n' to kana
  Press "'" for small-tsu. (type "chotto" as "cho'to")
  Press "-" for kana elongation. ("bi-ru")
  Press backspace to erase the last char. 
  Press return to start the search.

For a pointy-clicky method of input, press the "kana search"
button. This will pop up the kana seach window. (Press it again to remove
the window.)  
Press the kana(s) you want to search for. The chars you press will be shown
next to the "kana search" button in the main search window. When you have
the phrase ready, press the [Search] box.

If your kana recognition isn't all it should be, you can toggle romaji mode
in the options popup (via "options" from the main window).  Additionally, if
you don't know katakana, but want to translate a katakana phrase, use the
<=> button to toggle between hiragana and katakana.  Note that even if you
are in katakana input mode, it will print out your buttonpresses as
hiragana.  This is because the search engine treats hiragana and katakana
identically.

If you make a mistake, press the <- button, or backspace, to erase the last
char.

The characters you press will appear at the bottom of the popup, and also on
the main search window next to the kana search button. As noted above, if
you make a mistake typing, use the <- button on the kana window to erase, or
the backspace key.

.TP
\fBKANJI SEARCH\fR

You now have a multiple ways to look up Kanji.


.TP
4-corner Kanji search

If you want to find a kanji by shape, press the kanji search button on
the search window. This will bring up the kanji search window.
Press it again to remove the kanji search window.

This window employes the "4-corner method" of lookup.
The 4-corner method has lots of strange rules to it. I strongly recommend
that you read the description that comes in the kanjidic document file. It
is impossible for me to cover all details here.

In brief, you have to press each corner of the center box, and select one of
the ten elements from the top row, that best matches that corner of the
kanji you want to look up. For those already familiar with the
4-corner method, the "blank" element is an alias for the first element.
There are still only 10 possible positions.

Press the paragraph button (backwards 'P') when you are ready to search.

For those NOT already familiar with the 4-corner method... unfortunately, it
sounds easy, but it is really horribly difficult, and I again refer you to
the documentation that comes with the kanjidic dictionary file. Look for
"kanjidic.doc"

.TP
SKIP Kanji search

Pressing the "Kanji SKIP search" button, will bring up the SKIP window.
This window has directions on it already. Follow the directions to
define what the kanji looks like.

.TP
Kanji cut-n-paste lookup

For ELECTRONIC lookup... if you view Japanese text online with a program
like "kterm", you can now select a single kanji in kterm, and paste it into
a special "drop target" in the search window. It is to the far right of the
"kanji" search button.

If you highlight multiple characters, kdrill will now only look for
an exact match of all characters you paste in. (up to 4 chars).
Multi-char matching will NOT WORK, unless you have downloaded the
additional dictionary, "edict"

Note: There is a BUG in some versions of netscape 4.x. If you are viewing
kanji in a frame, you can seemingly highlight a character, but it will not
cut-n-paste to kdrill, or anywhere else. If this occurs, use right-click to
"open frame in new window", where you will be able to use cut-n-paste.
Cut-n-paste from netscape was also improved in version 5.9.6
.TP
\fBMATCH SIMILAR KANJI\fR

If you have a kanji already showing in the search window, and you are using
the 'edict' dictionary, you can search the large dictionary for occurences of
the current kanji. Press the "match" button next to the kanji display.

.TP
What are all those letters?

The top row; "G, F, #", all refer to the basic indexes that are shown in
the mail kdrill window. They stand for "Grade, Frequency, and Index #",
respectively.

\fBH\fR denotes the index in the "Halperin" dictionary

\fBN\fR denotes the index in the "Nelson" dictionary

\fBUx\fR denotes the "Unicode" of the kanji. It is U\fBx\fR to make it
stand out as the only one that expects input in Hexadecimal. This is
because that is the way the dictionary has it.

For all windows with the little ^ in them, you can change the values.
When you press return or enter in them, kdrill will attempt to find a
match for what you just entered. If it can find no match, it will blank
out all fields displayed.

You can use this jump-to-index feature in the main window too. However,
the main window will keep any restrictions you might have while doing the
search (limits by usefile, grade, or frequency limit).

The search window ignores any restrictions on the main window, and
searches the entire on-line dictionary.

.SH KEYBOARD ACCELERATORS
Almost everything has a keyboard shortcut in kdrill.

    Key                               Action
  1,2,3,4,5                  Make a guess
  Shift+(123456+)            Change grade levels used
  c                          (C)heat
  C                          Super(C)heat
  e                          Guess (e)nglish definision
  k                          Guess which (k)anji fits
  m                          Guess which kana (m)eaning fits
  E                          Quiz on (E)nglish
  K                          Quiz on (K)anji
  M                          Quiz on kana (M)eaning
  l                          popup (l)earn window
  n                          (n)ext char, IF in learn window
  o                          Toggle in-(o)rder drill
  O                          Bring up (O)ptions window
  p                          Go back to (p)revious
  Control+q                  (Q)uit kdrill
  u                          Toggle (u)sefile usage.
  s                          (S)earch for a Kanji
  T                          Timer start/stop
  x                          clear missed count
  

Additionally, the Sun keyboard "Find" (F19) and "Props" (F13) keys
are bound to the search and options windows, respectively.

.SH CONFIGURATION DETAILS
kdrill checks for a file by the name of
.B ".kanjiusefile"
in the current directory, although this name can be changed either with
the '-usefile' option, or in a resource file.

kdrill also checks for a logfile, named
.B kdrill.log
by default, in the current directory. This can be changed with the
-logfile option, or in a resource file. See "LOGFILES AND MISSED KANJI",
below.

kdrill uses a file called "kanjidic" (which does not come in the source
package) to interpret many of the various 16-bit kanji chars in the
.I kanji24
font supplied with the X11R5 distribution. This file should be in a place
accessible to all users. Normally it would be in /usr/local/lib or
somewhere similar.

.I kanjidic
subdivides its entries into grade levels, and frequency ratings. Grade
levels are similar to school grade levels, but more compressed. For
kdrill's purposes, grade levels start at 1, and increase to 6. There are
many kanji that do not have a grade level, due to their infrequency of
use, or other reasons. These are denoted by the "+" character in the
grade select window. 

The user can restrict the range of kanji to drill on in different, yet
compatible, ways. The first way is to make a ".kanjiusefile" 
with a an explicit list of desired kanji. 
(described above in "USEFILES"),

Changes you make to the "Grade Select" window or the "Frequency Range"
section will not do anything until you supply the correct match to the
current drill-question (or press the english/kana toggle button).

Keep in mind that the xfd font tool and other applications may refer to
kanji characters by a \fIhexadecimal\fR number. You may enter a
hexadecimal number by starting it with "0x". For example, "0x315c".
To maintain compatibility with the dictionary, the kdrill "usefile"
expects hexadecimal input, not decimal input. Similarly, the logfile
also stores kanji in hexadecimal format. This makes it easy to use a log
file of kanji you have missed as a usefile, for repeated drilling.

.SH LOGFILES AND MISSED KANJI
Every time you guess incorrectly, kdrill makes a note. It later will give
you extra practice on ones you missed, if you are playing in random order.
It will only repeat a missed character about 25% of the time. 
The more you miss a particular character, the more kdrill will repeat
showing it to you.

You can store a list of your incorrect answers by pressing the "Log"
button. kdrill will then write out all the kanji characters it thinks you
do not know into the logfile. This will erase any information previously
in that logfile. kdrill will also automatically update the logfile when
it quits The next time you start up kdrill, it will automatically
read in the logfile, if it exists.

The logfile is named "kdrill.log", by default. You may change the name
of the logfile with the -logfile option. 

It is  a good idea to press "Log" just before quitting kdrill.
That way, it will remember which characters you are weak on, for the next
time you play. It will then go back to those characters from time to
time, if you play in "random" order. If you do not press "Log", kdrill
will not save a record of what you have missed.

Alternatively, you can use the logfile as a usefile. kdrill will then
only quiz you on those kanji you missed. If you choose to do this, it is
a good idea to copy the log file over to a different file. That way, you
can make a logfile for your new usefile. For example, in UNIX;

cp kdrill.log kdrill.usefile
kdrill -usefile kdrill.usefile

The total number of missed entries is shown in the main window. If there
are just too many for your comfort (learning new kanji can be difficult!)
you can ERASE THE COUNT with your backspace or delete key.

.SH KANJIDIC and EDICT
The dictionary for kdrill, kanjidic, is currently available where it
originated, via ftp from
.I ftp.monash.edu.au,
or from a mirror in the U.S. at ftp.uwtc.washington.edu. Likewise for the
"edict" dictionary.  There are many other mirror sites mentioned on the
kdrill web page. (See below)

At the monash site, both the dictionary and this program can currently be
found in
.I /pub/nihongo

This program's primary ftp site is now ftp.bolthole.com.

There is also an official kdrill URL;
.TP
 http://www.bolthole.com/kdrill/

This currently shows you some screen-shots, and mentions the ftp sites.

.SH BUGS
"kanjidic" isn't perfect. There are "incomplete" entries, missing either
English or kana translations. There are also entries consisting of "See
Nxxxx", which isn't really an improvement. Note that you can
now use the search window to follow those "See Nxxxx" references!
[ Just search for that Nxxx, as if doing a search for English ]

Likewise, this man page may be incomplete!

.SH AUTHOR
Philip P. Brown

(Who has finally taken a format Japanese lesson! Which helped a lot,
 but am now back on the slow "self-taught" track. sigh! shikatta ga nai)

.SH COPYRIGHT
This program was originally created while I was a student at the
University of California. However, this program was developed entirely by
myself, on my own computer, not related to any classwork. I retain sole
right to this program. 

I, Philip Brown, hereby give permission to use, and/or modify this code, so
long as it it not sold for profit, and I am given credit somewhere in
the code. Unrelated works originally derived from this code are not
covered by this restriction (although it would be nice to mention me!)

.SH NOTICE
Send donations, postcards, muffins, letters of commendation, to

.RS 5
  Philip Brown
  5353 Josie Ave
  Lakewood, CA 90713
  USA
.RE

[ I HAVE received some nice email, and more is always welcome. No postcards,
 though. Sniff... Although I DID actually receive a small donation.
 Yaaay! I can buy more manga now! :-> ]

Bug reports always "welcome". However, please ensure that you can
reproduce it, so I can fix it for you.
Also, be sure to let me know your machine type, and version of kdrill you
are using.

Philip Brown

phil@bolthole.com
  
http://www.bolthole.com/

http://www.bolthole.com/kdrill/

SEE ALSO:

ftp://ftp.bolthole.com/kdrill/zidian.README
for information on how to use kdrill for Chinese learning
