#ifndef _GAME_H
#define _GAME_H

extern TRANSLATION lastpicked;
extern int truevalue;
extern int numberincorrect, totalguessed, totalmissed;

/* enum for modes of buttons */
enum { GUESS_ENGLISH, GUESS_KANJI,GUESS_KANA};

extern int choicesmode,questionmode;
extern int romajiswitch;

extern  TRANSLATION values[NUMBEROFCHOICES];

extern void CountKanji();
extern void SetupGuess();
extern void TallyWrong();
extern void ClearMissed();

extern TRANSLATION pickkanji(); /* only for use from badguess.c */

extern void BackCallback(Widget,XtPointer,XtPointer);
extern void ChangeMode(Widget,XtPointer,XtPointer);
extern void ChangeQuestion(Widget,XtPointer,XtPointer);
extern void Guessvalue(Widget,XEvent *,String *,Cardinal *);
extern void choicescallback(Widget,XtPointer,XtPointer),
	    cheatcallback(Widget,XtPointer,XtPointer);

extern void setenglabel(Widget,TRANSLATION);	/* set english button widgets */
extern void setkanjilabel(Widget,TRANSLATION);	/* set english button widgets */
extern void setkanalabel(Widget,TRANSLATION);	/* set english button widgets */
extern void printallchoices(), printquestion();

extern TRANSLATION picktruevalue();
extern void guessvalue(int);
extern void supercheat();

extern Boolean UseThisKanji(int kanjinum);

/* This function is declared in externs.h, but should be here, sort of
*    extern void setstatus(char *);
*/

#endif /* _GAME_H */
