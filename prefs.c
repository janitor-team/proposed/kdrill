/* This file handles retrieval and modification of "resources", which
 * can be considered "preferences".
 * There are two sets: global, and user-specific.
 * The global prefs go in the KDrill file
 *
 * The user-specific stuff is handled by an additional file,
 *  $HOME/.kdrill, in Xresource format. (like KDrill.ad)
 * We update that file with the latest values, when we quit.
 * Unless "NOXRMSAVE" is defined. In which case, we only
 * read the globals, and do not try to save. 
 * 
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>	/* for POSIX_PATH_MAX */

#include <Xlib.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xresource.h>

#include "defs.h"
#include "externs.h"


/* GetXtrmNumber(), GetXtrmBoolean(), GetXtrmString():
 *	Not the "best way" to get resources...
 *	supposed to read them all in at one go.
 *	But I had reasons for reading things in at different times,
 *	and I have stuck to that model
 */

/* GetXtrmString:
 *	Given the resource name and class, will
 *	 copy the resource string value to
 *	 destinationstring address.
 *	Destination must be MALLOCED ARRAY!!!
 *      (or rather, not just a pointer?)
 *
 *	Default value of string is "NOT SET".
 *	This is used by initlog() [which is not used any more]
 *	This should actually be encountered. I provide default values for
 *	all resource strings used.
 */
void GetXtrmString(char *name,char *rec_class, char *destinationstring)
{
	char resourcename[100],resourceclass[100];
	char buffer[100];
	char *returnstring = buffer;
	

	XtResource resourceList ={
		NULL,NULL,
		XtRString,sizeof(char *),0,
		XtRString,"NOT SET"
	};
	resourceList.resource_name = resourcename;
	resourceList.resource_class = resourceclass;

	strcpy(resourcename,name);
	strcpy(resourceclass,rec_class);

	XtGetApplicationResources(toplevel,&returnstring,
				  &resourceList,1,
				  NULL,0);
	strcpy(destinationstring,returnstring);
}

/* GetXtrmNumber:
 *	Given resource name and and class, will attempt to look up
 *	resource value in database.
 *	Will return default of 0!!!
 */
int GetXtrmNumber(char *name,char *rec_class)
{
	char resourcevalue[100];

	GetXtrmString(name,rec_class,resourcevalue);
	return atoi(resourcevalue);
}
/* GetXtrmBoolean()
 *	see GetXtrmNumber.
 *
 *	default of False
 */
Boolean GetXtrmBoolean(char *name,char *rec_class)
{
	Boolean resourcevalue;
	char resourcename[100],resourceclass[100];

	XtResource resourceList ={
		NULL,NULL,
		XtRBoolean,sizeof(Boolean),0,
		XtRBoolean,False,
	};
	resourceList.resource_name = resourcename;
	resourceList.resource_class = resourceclass;

	strcpy(resourcename,name);
	strcpy(resourceclass,rec_class);

	XtGetApplicationResources(toplevel,&resourcevalue,
				  &resourceList,1,
				  NULL,0);
	return resourcevalue;
}


void SetXtrmString(char *string, char *value)
{
#ifndef NOXRMSAVE
	char namebuff[100];
	char *pstring=namebuff;
	XrmDatabase db = XrmGetDatabase(display);
	if(db==NULL){
		perror("could not get x resource db?");
		return;
	}
	if(string[0]!='K'){
		sprintf(namebuff, "KDrill.%s",string);
	} else{
		strcpy(namebuff, string);
	}

	/*pstring=(char*)malloc(strlen(namebuff)+1);
	strcpy(pstring,namebuff);
	*/
	XrmPutStringResource(&db, pstring, value);
#ifdef DEBUG
	printf("Set %s to %s\n", pstring, value);
#endif

#endif /*NOXRMSAVE*/

}

void SetXtrmNumber(char *string, int value)
{
	char svalue[10];
	sprintf(svalue,"%d", value);
	SetXtrmString(string, svalue);
}

void SetXtrmBoolean(char *string, Boolean value)
{
	char svalue[10];
	if (value==False)
		sprintf(svalue,"False");
	else
		sprintf(svalue,"True");

	SetXtrmString(string, svalue);
}

#ifndef _POSIX_PATH_MAX
#define _POSIX_PATH_MAX 1024
#endif


/*
 * Save our options, in Xrm format, in the user's home directory.
 * We can only save stuff if values have been set with the SetXtrmXXXX()
 * convenience routines elsewhere in this file.
 */
void SaveConfig()
{
#ifndef NOXRMSAVE
	FILE *preffile;
	char fname[_POSIX_PATH_MAX];
	char *savelist[]=
	{
		"noBell",
		"nousefile",
		"usefile",
		"kanjifont",
		"smallkanji",
		"englishfont",
		"kdictfile",
		"edictfile",
		"radkfile",
		"gradelevel",
		"guessmode",
		"questionmode",
		"romajiswitch",
		"lowfrequency",
		"highfrequency",
		"showinorder",
		"timersec",
		"logfile",
		NULL
	};
	char **rname=savelist;

	sprintf(fname,"%s/.kdrill", homedir);
	preffile=fopen(fname,"w");
	if(preffile==NULL){
		perror("could not open pref file .kdrill");
		return;
	}
	fprintf(preffile, "! Do not hand-edit this file\n");
	fprintf(preffile, "! It will get overwritten when you quit kdrill\n");
	
	while(*rname!=NULL){
		char classbuff[100], infobuff[100];
		sprintf(classbuff,*rname);

		GetXtrmString(*rname, classbuff,infobuff);
		if(strcmp(infobuff, "NOT SET")==0){
			rname++;
			continue;
		}
#ifdef DEBUG
		printf("Saving pref %s: %s\n",*rname, infobuff);
#endif
		fprintf(preffile, "KDrill.%s: %s\n", *rname, infobuff);
		rname++;
	}
	fclose(preffile);

#endif /* NOXRMSAVE*/
}
