/* "mainwindow.c"
 *	Has all the widget management routines for the
 *	main-window english/widget widgets.
 */

#include <stdio.h>
#include <stdlib.h>

#include <ctype.h>
#include <Intrinsic.h>
#include <StringDefs.h>
#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>

#include "defs.h"
#include "game.h"
#include "widgets.h"
#include "searchwidgets.h"
#include "options.h"
#include "utils.h"
#include "externs.h"

/* Keeps track of if we are using 'cheat' to have a button highlighted */

static int cheat_on=0;

/* BackCallback:
 *	go back to last kanji.
 *  Has to be in game.c, to access static vars
 */
void BackCallback(Widget w,XtPointer client_data,XtPointer call_data)
{
	TRANSLATION buffer;

	if(lastpicked == 0) return;

	buffer= values[truevalue];
	values[truevalue] = lastpicked;
	lastpicked = buffer;

	setstatus("Going to previous character");
	printquestion();
	printallchoices();
	DescribeCurrent(values[truevalue]);
	
}

/* changemode:
 *	Change from guessing kanji to guessing english, or vica verse,
 *	 by clicking on the appropriate button
 *	Which button has been clicked, is indicated in the
 *	  "data" field.
 *
 *   Also handles changing from english <->kana display,
 *   calling CountKanji(), because we have to .
 */
void ChangeMode(Widget w,XtPointer data,XtPointer calldata)
{
	/* whichbutton is
	 * EITHER
	 *    [mode 4kanji]<->[mode 4english] button,
	 * OR
	 *   [change  kana<->english]
	 */
	int buttonpressed = (int) data;
#ifdef DEBUG
	printf("button mode change: %d\n",buttonpressed);
#endif
	switch(buttonpressed){
		case GUESS_ENGLISH:
			SetXtrmString("guessmode", "english");
			break;
		case GUESS_KANJI:
			SetXtrmString("guessmode", "kanji");
			break;
		case GUESS_KANA:
			SetXtrmString("guessmode", "kana");
			break;
		default:
			fprintf(stderr,"Error: cannot switch to non-existant mode\n");
			return;
	}
	choicesmode = buttonpressed;
	if(switchKanaEnglish){ /* allow switching in place */
		printallchoices();
	} else {
		CountKanji();
		SetupGuess();
	}
}


/* Change the "Question" label at the top of the main window,
 * between kanji, kana, and english display
 */
void ChangeQuestion(Widget w,XtPointer data,XtPointer calldata)
{
	int buttonpressed = (int) data;

#ifdef DEBUG
	printf("changing question display\n");
#endif
	questionmode = buttonpressed;
	switch(questionmode){
		case GUESS_ENGLISH:
			SetXtrmString("questionmode", "english");
			break;
		case GUESS_KANJI:
			SetXtrmString("questionmode", "kanji");
			break;
		case GUESS_KANA:
			SetXtrmString("questionmode", "kana");
			break;
	}
	printquestion();
}

/* Guessvalue:
 *	accelerator function so we can guess with 1,2,3,4 keys
 *	NOTE:
 *	we get passed the value (key-1)
 */
/* note that "-1" means "default due to time" */
void Guessvalue(Widget w,XEvent *event,String *params,Cardinal *num_parags)
{
	int value;
	value = atoi(*params);
	if(value >=NUMBEROFCHOICES) return;
	guessvalue(value);
}


/* This is called by both button and key-accelerator.
 * Either sets the 'cheat' highlight, or turns it off, if already set.
 * set client_data to non-NULL if you dont want status message set
 */
void cheatcallback(Widget w,XtPointer client_data,XtPointer calldata)
{
	ReverseButton(choicesWidgets[truevalue]);
	cheat_on=!cheat_on;
	printallchoices();

	if((client_data ==NULL) && cheat_on){
		setstatus("Try Harder!");
	}
}

/* This is currently ONLY called by key-accelerator
 * Do BOTH highlight of correct value, AND pop up search window
 * with full details of that kanji
 */
void supercheat()
{
	cheatcallback(NULL,NULL,NULL);
	printsearch(values[truevalue]);
}

/* Hook for guessvalue in game.c to clear 'cheat' highlighting */
void ClearCheat()
{
	if(!cheat_on) return;

	ReverseButton(choicesWidgets[truevalue]);
	cheat_on=0;
}

/* choicescallback:
 *	Handles clicking on guess
 */
void choicescallback(Widget w,XtPointer data,XtPointer calldata)
{
	guessvalue((int) data);
}

