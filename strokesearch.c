#include <stdio.h>

#include <Xos.h>
#include <Xfuncs.h>
#include <Intrinsic.h>
#include <StringDefs.h>

#include <Shell.h>

#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"

#include "search.h"
#include "searchwidgets.h"
#include "convert.h"
#include "game.h"
#include "readfile.h"
#include "utils.h"
#include "init.h"

/***************************************************/
/* We implement "stroke count search" here.	   */
/***************************************************/


Widget kanjistroke_popup;

enum {SKIP_RL, SKIP_UD, SKIP_ENC, SKIP_DK, SKIP_NUMW};

static Widget skiptoggle[5];
static short skipval;

Widget strokeinputw;

/*
 * Get the number of strokes input, and pass along to appropriate
 * kanji search routine.
 * Also use skip value, if present.
 */
void DoStrokeSearch(Widget w, XtPointer data, XtPointer call_data)
{
	int strokecount;
	strokecount=GetWidgetNumberval(strokeinputw);
	if(strokecount<1) {
		XtVaSetValues(strokeinputw,XtNstring,"",NULL);
		return;
	}
	
	dostrokefind(strokecount,skipval);

}

/* handle toggle for additional narrowing parameter on search.
 * we cross-reference with initial SKIP number
 */
void StrokeSKIPcount(Widget w, XtPointer data, XtPointer call_data)
{
	int bcount;
	for(bcount=0;bcount<SKIP_NUMW;bcount++){
		UnhighlightButton(skiptoggle[bcount]);
	}

	HighlightButton(w);
	if(w == skiptoggle[SKIP_RL]){
		skipval=skipfromthree(1,0,0);
	}
	if(w == skiptoggle[SKIP_UD]){
		skipval=skipfromthree(2,0,0);

	}
	if(w == skiptoggle[SKIP_ENC]){
		skipval=skipfromthree(3,0,0);
	}
	if(w == skiptoggle[SKIP_DK]){
		skipval=0;
	}
}

static char * strokeAccel = 
 " <Key>Return:  stroke-search()";

/*
 * make the widgets for the "search by stroke count" kanji search method
 * It also makes buttons for our pseudo-"SKIP" coding cross reference.
 *
 */
void makestrokeinput(Widget parent)
{
	XtAccelerators Accel;
	int bcount;
	Widget slabel;
	Widget skiplabel;
	Widget leftform, rightform;
	Widget gosearch;

	leftform=XtVaCreateManagedWidget("leftform", formWidgetClass,
		  parent,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
		  NULL);
	slabel=XtVaCreateManagedWidget("strokelabel",
				labelWidgetClass, leftform,
				XtNborderWidth,0,
				XtNlabel, "Stroke count",
				NULL);

	strokeinputw=XtVaCreateManagedWidget("strokecount",
			asciiTextWidgetClass, leftform,
			XtNfromVert,slabel,
			XtNwidth,40,
			XtNeditType,XawtextEdit,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
				NULL);

	rightform=XtVaCreateManagedWidget("rightform", formWidgetClass,
		parent,
		XtNfromHoriz,leftform,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop,XawChainTop,
			XtNbottom, XawChainTop,
		NULL);

	skiplabel=XtVaCreateManagedWidget("skiplabel", labelWidgetClass,
				rightform,XtNlabel,"Shape of kanji",
				XtNborderWidth,0,NULL);

	skiptoggle[SKIP_RL]=XtVaCreateWidget("strokeskip0",
				commandWidgetClass,
				rightform,
				XtNlabel,"l/r",
				XtNfromVert,skiplabel,
				NULL);
	skiptoggle[SKIP_UD]=XtVaCreateWidget("strokeskip1",
				       commandWidgetClass,
				       rightform,
				       XtNlabel,"u/d",
				       XtNfromVert,skiplabel,
				       XtNfromHoriz,skiptoggle[SKIP_RL],
				       NULL);
	skiptoggle[SKIP_ENC]=XtVaCreateWidget("strokeskip2",
				       commandWidgetClass,
				       rightform,
				       XtNlabel,"enc",
				       XtNfromVert,skiplabel,
				       XtNfromHoriz,skiptoggle[SKIP_UD],
				       NULL);
	skiptoggle[SKIP_DK]=XtVaCreateWidget("strokeskip3",
				       commandWidgetClass,
				       rightform,
				       XtNlabel,"dontknow",
				       XtNfromVert,skiptoggle[SKIP_RL],
				       NULL);

	gosearch=XtVaCreateManagedWidget("strokesearch", commandWidgetClass,
		  parent,
		  XtNlabel,"search",XtNfromVert,leftform,
		XtNleft,XawChainLeft,
		XtNright,XawChainLeft,
		XtNtop, XawChainTop,
		XtNbottom, XawChainTop,
		  NULL);

	XtManageChildren(&skiptoggle[0],SKIP_NUMW);

	for(bcount=0;bcount<SKIP_NUMW;bcount++){
		XtAddCallback(skiptoggle[bcount],
		      XtNcallback, StrokeSKIPcount, NULL);
	}
	XtAddCallback(gosearch, XtNcallback, DoStrokeSearch, (XtPointer) NULL);

	Accel = XtParseAcceleratorTable(strokeAccel);
	XtOverrideTranslations(strokeinputw, Accel);

	StrokeSKIPcount(skiptoggle[SKIP_DK],NULL,NULL);

}

/* exported routine */
void MakeStrokeinputPopup()
{
	Widget strokeinputform;

	kanjistroke_popup = XtVaCreatePopupShell("kdrill_strokesearch ",
		topLevelShellWidgetClass,
		search_popup,
		NULL);

	strokeinputform = XtVaCreateManagedWidget("strokeinputform",
					     formWidgetClass,
					     kanjistroke_popup,
					     NULL);

	/* make all the substuff */
	makestrokeinput(strokeinputform);

}

/*
 * Exported routine:
 * 
 * It pops up the kanji strokecount search window
 */
void Showstroke(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int stroke_up = -1;

	static Position rel_x,rel_y;
	Position x,y;


	if(stroke_up==-1){
		/* first time init.. */
		/*rel_x = GetXtNumber("kanjistroke_popupx","Search_popupx");*/
		/*rel_y = GetXtNumber("kanjistroke_popupy","Search_popupy");*/
		rel_x = 10;
		rel_y = 10;
		stroke_up=0;
	}
	if(isMapped(kanjistroke_popup)==False){
		XtTranslateCoords(search_popup,rel_x,rel_y,&x,&y);
		XtVaSetValues(kanjistroke_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);
		XtPopup(kanjistroke_popup,XtGrabNone);
		if(stroke_up==0){
			setup_deletewindow(kanjistroke_popup);
			stroke_up=1;
		}
		
		setstatus("Bringing up stroke input window...");
	} else {
		XtPopdown(kanjistroke_popup);
	}
}


