/* This module handles a timer for guessing. Guess within X seconds,
 * or you lose :-)
 *
 * There can be only one timeout currently in the program.
 * Length of timeout is changed in the options window, via the widgets
 * made by MakeTimeoutOptions()
 */

extern void StartTimeout();
extern int StopTimeout();
extern void EnableTimer(int on_off);
extern void setTimeoutLen(int newlen);
extern void TimerCallback(Widget w,XtPointer data,XtPointer calldata);

extern void MakeTimeoutOptions(Widget parentForm);

/* This is for keyboard accelerator */
extern void UpdateTimeout(Widget w,XEvent *,String *,Cardinal *);
