
/* kanasearch.c
 *	Creates and handles display of the special kana input window.
 * 
 *	Also creates and handles display the (four-corner method)
 *	 kanji search window.
 *
 *	Both are only a subwindow of the main search window popup.
 *	Actual searching is done in search.c
 */

#include <stdio.h>

#include <Xos.h>
#include <Xfuncs.h>
#include <Intrinsic.h>
#include <StringDefs.h>

#include <Shell.h>

#include <Xaw/Command.h>
#include <Xaw/Label.h>
#include <Xaw/Form.h>
#include <Xaw/Box.h>
#include <Xaw/AsciiText.h>

#include "defs.h"
#include "externs.h"

#include "search.h"
#include "utils.h"
#include "searchwidgets.h"
#include "convert.h"
#include "game.h"
#include "init.h"


Widget kanawidgets[NUM_OF_KW];
Widget kanainput_popup;
Widget romajiinput;

static int max_kana=0;


XChar2b std_translations[NUM_OF_KW][2]=
{
	{{0x24, 0x22}, {0x0, 0x0}},	/* a */
	{{0x24, 0x24}, {0x0, 0x0}},
	{{0x24, 0x26}, {0x0, 0x0}},
	{{0x24, 0x28}, {0x0, 0x0}},
	{{0x24, 0x2a}, {0x0, 0x0}},

	{{0x24, 0x2b}, {0x0, 0x0}},	/* ka */
	{{0x24, 0x2d}, {0x0, 0x0}},
	{{0x24, 0x2f}, {0x0, 0x0}},
	{{0x24, 0x31}, {0x0, 0x0}},
	{{0x24, 0x33}, {0x0, 0x0}},

	{{0x24, 0x35}, {0x0, 0x0}},	/* sa */
	{{0x24, 0x37}, {0x0, 0x0}},
	{{0x24, 0x39}, {0x0, 0x0}},
	{{0x24, 0x3b}, {0x0, 0x0}},
	{{0x24, 0x3d}, {0x0, 0x0}},

	{{0x24, 0x3f}, {0x0, 0x0}},	/* ta */
	{{0x24, 0x41}, {0x0, 0x0}},
	{{0x24, 0x44}, {0x0, 0x0}},
	{{0x24, 0x46}, {0x0, 0x0}},
	{{0x24, 0x48}, {0x0, 0x0}},

/*20*/	{{0x24, 0x4a}, {0x0, 0x0}},	/* na */
	{{0x24, 0x4b}, {0x0, 0x0}},
	{{0x24, 0x4c}, {0x0, 0x0}},
	{{0x24, 0x4d}, {0x0, 0x0}},
	{{0x24, 0x4e}, {0x0, 0x0}},

	{{0x24, 0x4f}, {0x0, 0x0}},	/* ha */
	{{0x24, 0x52}, {0x0, 0x0}},
	{{0x24, 0x55}, {0x0, 0x0}},
	{{0x24, 0x58}, {0x0, 0x0}},
	{{0x24, 0x5b}, {0x0, 0x0}},

	{{0x24, 0x5e}, {0x0, 0x0}},	/* ma */
	{{0x24, 0x5f}, {0x0, 0x0}},
	{{0x24, 0x60}, {0x0, 0x0}},
	{{0x24, 0x61}, {0x0, 0x0}},
	{{0x24, 0x62}, {0x0, 0x0}},

	{{0x24, 0x69}, {0x0, 0x0}},	/* ra */
	{{0x24, 0x6a}, {0x0, 0x0}},
	{{0x24, 0x6b}, {0x0, 0x0}},
	{{0x24, 0x6c}, {0x0, 0x0}},
	{{0x24, 0x6d}, {0x0, 0x0}},

	{{0x24, 0x2c}, {0x0, 0x0}},	/* ga */
	{{0x24, 0x2e}, {0x0, 0x0}},
	{{0x24, 0x30}, {0x0, 0x0}},
	{{0x24, 0x32}, {0x0, 0x0}},
	{{0x24, 0x34}, {0x0, 0x0}},

	{{0x24, 0x36}, {0x0, 0x0}},	/* za */
	{{0x24, 0x38}, {0x0, 0x0}},
	{{0x24, 0x3a}, {0x0, 0x0}},
	{{0x24, 0x3c}, {0x0, 0x0}},
	{{0x24, 0x3e}, {0x0, 0x0}},

/*50*/	{{0x24, 0x40}, {0x0, 0x0}},	/* da */
	{{0x24, 0x42}, {0x0, 0x0}},
	{{0x24, 0x45}, {0x0, 0x0}},
	{{0x24, 0x47}, {0x0, 0x0}},
	{{0x24, 0x49}, {0x0, 0x0}},

	{{0x24, 0x50}, {0x0, 0x0}},	/* ba */
	{{0x24, 0x53}, {0x0, 0x0}},
	{{0x24, 0x56}, {0x0, 0x0}},
	{{0x24, 0x59}, {0x0, 0x0}},
	{{0x24, 0x5c}, {0x0, 0x0}},

	{{0x24, 0x51}, {0x0, 0x0}},	/* pa */
	{{0x24, 0x54}, {0x0, 0x0}},
	{{0x24, 0x57}, {0x0, 0x0}},
	{{0x24, 0x5a}, {0x0, 0x0}},
	{{0x24, 0x5d}, {0x0, 0x0}},

	/* and now the wierd stuff */
	
/*65*/	{{0x24, 0x64}, {0x0, 0x0}},	/* ya */
	{{0x24, 0x79}, {0x0, 0x0}},	/* blank */
	{{0x24, 0x66}, {0x0, 0x0}},	/* yu */
	{{0x24, 0x79}, {0x0, 0x0}},	/* blank */
	{{0x24, 0x68}, {0x0, 0x0}},	/* yo */

	{{0x24, 0x63}, {0x0, 0x0}},	/* small ya */
	{{0x24, 0x6e}, {0x0, 0x0}},	/* small wa */
	{{0x24, 0x65}, {0x0, 0x0}},	/* small yu */
	{{0x24, 0x79}, {0x0, 0x0}},	/* blank */
	{{0x24, 0x67}, {0x0, 0x0}},	/* small yo */

	{{0x24, 0x6f}, {0x0, 0x0}},	/* wa */
	{{0x21, 0x3c}, {0x0, 0x0}},	/* '--' */
	{{0x24, 0x43}, {0x0, 0x0}},	/* small tsu */
	{{0x24, 0x73}, {0x0, 0x0}},	/* N */
	{{0x24, 0x72}, {0x0, 0x0}},	/* 'o' */


	{{0x22, 0x2b}, {0x0, 0x0}},	/* <- */

	{{0x0, 0x0}, {0x0, 0x0}}, /* marker for "automated" widget creation */
	

	{{0x22, 0x79}, {0x0, 0x0}},	/* paragraph [accept input]*/
					/* match this in process_kinput */
	{{0x22, 0x4e}, {0x0, 0x0}}	/* kata/hiragana mode toggle */
};

XChar2b paragraphglyph[2] = { {0x22, 0x79}, {0x0, 0x0} };

/************************************************************/

/*
 * static char *searchAccel = 
 * " <Key>Return:  do-find()";
 */


/* This gets called after clicking on the "Kana search" button,
 * It pops up the appropriate specialized window
 */
void Showinputkana(Widget w,XtPointer client_data, XtPointer call_data)
{
	static int kana_up = -1;
	static Position rel_x,rel_y;
	Position x,y;

#ifdef DEBUG
	puts("Showinputkana.. Boom");
#endif

	if(kana_up==-1){
		/* first time init.. */
		rel_x = 10;
		rel_y = 10;
		kana_up=0;
	}
	if(isMapped(kanainput_popup)==False){
		

		XtTranslateCoords(search_popup,rel_x,rel_y,&x,&y);
		XtVaSetValues(kanainput_popup,
		      XtNx,x,
		      XtNy,y,
		      NULL);
		XtPopup(kanainput_popup,XtGrabNone);
		if(kana_up==0){
			setup_deletewindow(kanainput_popup);
			kana_up=1;
		}
		setstatus("Bringing up kana input window...");
	} else {
		XtPopdown(kanainput_popup);
	}
}




/* printkanatable
*	print out kana table, according to current style
*	(standard order, simple order, hiragana/vs/katakana
*/
void printkanatable()
{

	if(isMapped(kanainput_popup)==False){
		/* pop up window, if it isnt already */
		SearchCallback(kanainput_popup,NULL,NULL);
	}
}



/* we translate buttons-with-kana-on-them, to
 * a hiragana char here.
 * We then pass it on to process_kinput()
 */
void kanacallback(Widget w, XtPointer data, XtPointer call_data)
{
	XChar2b CHAR;

	CHAR.byte1 = ((int) data & 0xff00) >>8;
	CHAR.byte2 = ((int) data & 0x00ff);

	if(((CHAR.byte1 == 0x21) && (CHAR.byte2 == 0x21))
	   || CHAR.byte1 == 0)
	{
#ifdef DEBUG
		printf("Got blank... skipping...");
		printf("%x %x\n", CHAR.byte1, CHAR.byte2);
#endif
		return;
	}
	process_kinput(CHAR);
}

/*
 * ksearch_toggles() sets the labels for the point-n-click popup window
 * 
 * It has to deal with romaji, hiragana, or kanakana
 *
 * If call_data == 1, just listen to romajiswitch( romaji vs kana),
 *   and change all the labels 
 * If call_data == 2, 
 *    actually set global variable to toggle between hiragana and katakana
 *    and go set all the labels.
 *    (or leave as romaji, if romajiswitch)
 */

void ksearch_toggles(Widget w, XtPointer data, XtPointer call_data)
{
	static int popup_hirakana_mode=1;
	char romajibuff[MAXROMAJI+1];
	int kcount;

	if((int)data == 2){
		/* toggle static variable */
		popup_hirakana_mode = 1 - popup_hirakana_mode;

		
		if(romajiswitch ==1){
			/* nothing left to do. Stick with romaji*/
			return;
		}
	}

	/* if data != 2, assume ==1. */


	/* Now romajiswitch and popup_hirakana_mode are set properly.
	 * Interpret them.
	 */

	if(romajiswitch == 1){
		/* if romaji is set, it doesn't matter what the other is */
		for(kcount=0; kcount<80; kcount++){

			kanatoromaji(std_translations[kcount], romajibuff);

		 XtVaSetValues(kanawidgets[kcount],
			       XtNencoding,XawTextEncoding8bit,
			       XtNfont,englishfont,
			       /*  XtNlabel,romaji_translations[kcount],*/
			       XtNlabel, romajibuff,
			       NULL);

		}
		return;
	}
	if(popup_hirakana_mode == 1){
		/* need to switch to hiragana */
		for(kcount=0; kcount<80; kcount++){
			XtVaSetValues(kanawidgets[kcount],
				 XtNlabel, std_translations[kcount],
				 XtNencoding, XawTextEncodingChar2b,
				 XtNfont, smallkfont,
				 NULL);
		}
	} else {
		/* bump all the std_translations to katakana */
		XChar2b kstr[2]={{0x25,0},{0,0}};

		for(kcount=0; kcount<80; kcount++){
			/* skip symbol type buttons */
			if(std_translations[kcount][0].byte1!=0x24)
				continue;

			kstr[0].byte2 = std_translations[kcount][0].byte2;

			XtVaSetValues(kanawidgets[kcount],
				 XtNlabel, kstr,
				 XtNencoding, XawTextEncodingChar2b,
				 XtNfont, smallkfont,
				 NULL);
		}
	}
}


/*************************************************************
 *    Widget Creation Below Here                             *
 *************************************************************/

/* This is for the point-n-click kana input window */
void makeinputwidgets(Widget parent)
{
	int kvalue;
	Widget searchbutton;

	max_kana=0;

	while((std_translations[max_kana][0].byte1 != 0x00)){
		char namestr[10];

		sprintf(namestr, "%d", max_kana);
		kanawidgets[max_kana] =
			XtVaCreateWidget(namestr,
				commandWidgetClass,
				parent,
				XtNlabel, std_translations[max_kana],
				XtNencoding, XawTextEncodingChar2b,
				XtNfont, smallkfont,
				XtNleft,XawChainLeft,
				XtNright,XawChainLeft,
				XtNtop, XawChainTop,
				XtNbottom, XawChainTop,
				NULL);

		if(max_kana>4){
			XtVaSetValues(kanawidgets[max_kana],
				      XtNfromVert, kanawidgets[max_kana - 5],
				      NULL);
		}
		if(max_kana%5 >0){
			XtVaSetValues(kanawidgets[max_kana],
				      XtNfromHoriz, kanawidgets[max_kana - 1],
				      NULL);
		}

		kvalue = (std_translations[max_kana][0].byte1 <<8) +
				std_translations[max_kana][0].byte2;
		XtAddCallback(kanawidgets[max_kana],
			      XtNcallback, kanacallback,
			      (XtPointer) kvalue);
		max_kana++;
	}

	/*std_translations[max_kana] now points to 0x00,0x00*/
	XtManageChildren(kanawidgets, max_kana);

	max_kana+=1;

	/* create the kana/hiragana toggle */
	kanawidgets[max_kana+1] =
		XtVaCreateManagedWidget("kanahiratoggle", commandWidgetClass,
					parent,
					XtNlabel, std_translations[max_kana+1],
					XtNencoding, XawTextEncodingChar2b,
					XtNfont, smallkfont,
					XtNfromHoriz, kanawidgets[max_kana-2],
					XtNfromVert, kanawidgets[max_kana-5],
					XtNleft,XawChainLeft,
					XtNright,XawChainLeft,
					XtNtop, XawChainTop,
					XtNbottom, XawChainTop,
					NULL);
					
	searchbutton=XtVaCreateManagedWidget("searchkn",commandWidgetClass,
				parent,
				XtNlabel, "Search",
				XtNfont,englishfont,
				XtNfromVert,kanawidgets[max_kana-5],
				XtNfromHoriz, kanawidgets[max_kana+1],
				XtNhorizDistance,40,
			XtNleft,XawChainLeft,
			XtNright,XawChainLeft,
			XtNtop, XawChainTop,
			XtNbottom, XawChainTop,
				NULL);

	/* I HATE hardcoding paragraphglyph here, but it isnt working*/
	XtAddCallback(searchbutton,
		      XtNcallback, kanacallback,
		      (XtPointer) 0x2279);



	XtAddCallback(kanawidgets[max_kana+1],
		      XtNcallback, ksearch_toggles,
		      (XtPointer) 2);

	

	/**************************************************/
	/* This is a place to type romaji, like in the main search
	 * window. It's actually IDENTICAL to the main window.
	 * This is just a mirror copy.
	 */
	romajiinput = XtVaCreateManagedWidget("romajiinput", labelWidgetClass,
			parent,
			XtNwidth, 150,
			XtNfromVert, searchbutton,
			XtNencoding, XawTextEncodingChar2b,
			XtNlabel,"           \0\0",
			XtNfont, smallkfont,
			XtNcursor, textcursor,
			XtNleft,XawChainLeft,
			XtNright,XawChainRight,
			XtNtop, XawChainBottom,
			XtNbottom, XawChainBottom,
			NULL);
	/* make this a duplicate of the searchwidgets[SEARCH_KANA_W],
	 * so user can see output of point-n-click on same window.
	 * (or, user can even input romaji on that window!)
	 */
	XtAddEventHandler(romajiinput, KeyPressMask,
			  False, Handle_romajikana, NULL);


}


/* This is an exported "make widgets" routine */
void MakeKanainputPopup()
{
	Widget kanainputform;
	/*  XtAccelerators Accel; */

	kanainput_popup = XtVaCreatePopupShell("kdrill_kanainput",
		topLevelShellWidgetClass,
		search_popup,
		NULL);

	kanainputform = XtVaCreateManagedWidget("kanainputform",
					     formWidgetClass,
					     kanainput_popup,
					     NULL);

	makeinputwidgets(kanainputform);


	/*Accel = XtParseAcceleratorTable(searchAccel);*/
	/*XtOverrideTranslations(kanainputform, Accel);*/

}

